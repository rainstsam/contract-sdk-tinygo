/*
Copyright (C) BABEC. All rights reserved.

SPDX-License-Identifier: Apache-2.0

main_fact_cut.go
*/
package main

// 安装合约时会执行此方法，必须
//export init_contract
func initContract() {

}

// 升级合约时会执行此方法，必须
//export upgrade
func upgrade() {

}

//export save
func save() {
	// 获取参数
	txId, _ := GetTxId()
	time, _ := ArgString("time")
	fileHash, _ := ArgString("file_hash")
	fileName, _ := ArgString("file_name")

	// 组装
	stone := make(map[string]string, 4)
	stone["txId"] = txId
	stone["time"] = time
	stone["fileHash"] = fileHash
	stone["fileName"] = fileName

	// 序列化为json bytes
	items := ParamsMapToEasyCodecItem(stone)
	jsonStr := EasyCodecItemToJsonStr(items)

	// 存储数据
	PutState("fact", fileHash, jsonStr)
	// 返回结果
	SuccessResult("ok")
}

//export find_by_file_hash
func findByFileHash() {
	// 获取参数
	fileHash, _ := ArgString("file_hash")
	// 查询
	if result, resultCode := GetStateByte("fact", fileHash); resultCode != SUCCESS {
		// 返回结果
		ErrorResult("failed to call get_state, only 64 letters and numbers are allowed. got key:" + "fact" + ", field:" + fileHash)
	} else {
		// 记录日志
		LogMessage("get val:" + string(result))
		// 返回结果
		SuccessResultByte(result)
	}
}
func main() {

}
