package main

import (
	"chainmaker.org/contract-sdk-tinygo/sdk/convert"
	"strings"
)

// 安装合约时会执行此方法，必须

//export init_contract
func initContract() {
	LogMessage("[zitao] ========================================start")
	LogMessage("[zitao] input func: initContract")
	status, _ := ArgString("status")
	tblinfo, _ := ArgString("tblinfo")
	tblinfo_list := strings.Split(tblinfo, "##")
	/*
		CREATE TABLE Persons
		(
		Id_P int,
		LastName varchar(255),
		FirstName varchar(255),
		Address varchar(255),
		City varchar(255)
		)
	*/
	LogMessage("[zitao] change initContract[status]: " + status)
	LogMessage("[zitao] change initContract[tblinfo]: " + tblinfo)
	ctx := NewSqlSimContext()
	ctx.Log(tblinfo)

	for _, tblinfo_once := range tblinfo_list {
		_, resultCode := ctx.ExecuteDdl(tblinfo_once)
		if resultCode != SUCCESS {
			msg := "initContract error. resultCode=" + convert.Int32ToString(int32(resultCode)) + " tblinfo_once=" + tblinfo_once
			ctx.Log(msg)
			ctx.ErrorResult(msg)
			return
		} else {
			ctx.Log("create table teacher_gasm success.")
		}
	}

	if status == "success" {
		ctx.SuccessResult("create table success:" + tblinfo)
		ctx.Log("initContract success.")
	} else {
		ctx.ErrorResult("create table failure: " + tblinfo)
		ctx.Log("initContract [end]")
	}
}

//export upgrade
func upgrade() {
	LogMessage("[zitao] ========================================start")
	LogMessage("[zitao] input func: upgrade")
	status, _ := ArgString("status")
	tblinfo, _ := ArgString("tblinfo")
	tblinfo_list := strings.Split(tblinfo, "##")

	LogMessage("[zitao] change upgrade[status]: " + status)
	LogMessage("[zitao] change upgrade[tblinfo]: " + tblinfo)
	ctx := NewSqlSimContext()
	ctx.Log(tblinfo)

	for _, tblinfo_once := range tblinfo_list {
		_, resultCode := ctx.ExecuteDdl(tblinfo_once)
		if resultCode != SUCCESS {
			msg := "upgrade error. resultCode=" + convert.Int32ToString(int32(resultCode)) + " tblinfo_once=" + tblinfo_once
			ctx.Log(msg)
			ctx.ErrorResult(msg)
			return
		} else {
			ctx.Log("create table teacher_gasm success.")
		}
	}
	if status == "success" {
		ctx.SuccessResult("create table success:" + tblinfo)
		ctx.Log("upgrade success.")
	} else {
		ctx.ErrorResult("create table failure: " + tblinfo)
		ctx.Log("upgrade [end]")
	}
}

//export sql_handle_update
func sql_handle_update() {
	LogMessage("[zitao] ========================================start")
	LogMessage("[zitao] input func: sql_handle_update")
	status, _ := ArgString("status")
	tblinfo, _ := ArgString("tblinfo")
	LogMessage("[zitao] change sql_handle_update[status]: " + status)
	LogMessage("[zitao] change sql_handle_update[tblinfo]: " + tblinfo)
	ctx := NewSqlSimContext()

	tblinfo_list := strings.Split(tblinfo, "##")

	for _, tblinfo_once := range tblinfo_list {
		rowCount, resultCode := ctx.ExecuteUpdate(tblinfo_once)

		if resultCode != SUCCESS {
			ctx.Log("sql_handle_update error")
			ctx.ErrorResult("sql_handle_update error")
			return
		} else {
			msg := "sql_handle_update update row=" + convert.Int32ToString(rowCount)
			ctx.Log(msg)
		}
	}
	if status == "success" {
		ctx.SuccessResult("create sql_handle_update success:" + tblinfo)
		ctx.Log("sql_handle_update success.")
	} else {
		ctx.ErrorResult("sql_handle_update table failure: " + tblinfo)
		ctx.Log("sql_handle_update [end]")
	}
}

//export sql_handle_query
func sql_handle_query() {
	LogMessage("[zitao] ========================================start")
	LogMessage("[zitao] input func: sql_handle_query")
	status, _ := ArgString("status")
	tblinfo, _ := ArgString("tblinfo")
	LogMessage("[zitao] change sql_handle_query[status]: " + status)
	LogMessage("[zitao] change sql_handle_query[tblinfo]: " + tblinfo)
	tblinfo_list := strings.Split(tblinfo, "##")

	ctx := NewSqlSimContext()

	var result string
	for _, tblinfo_once := range tblinfo_list {
		resultSet, resultCode := ctx.ExecuteQuery(tblinfo_once)
		if resultCode != SUCCESS {
			ctx.Log("sql_handle_query error")
			ctx.ErrorResult("sql_handle_query error")
			return
		}

		for resultSet.HasNext() {
			ec, resultCode := resultSet.NextRow()
			if resultCode != SUCCESS {
				ctx.Log("NextRow error")
				ctx.ErrorResult("NextRow error")
				return
			}
			jsonStr := ec.ToJson()
			ctx.Log("NextRow: " + jsonStr)
			result += ","
			result += jsonStr
		}
		resultSet.Close()
	}
	result = strings.TrimLeft(result, ",")

	if status == "success" {
		ctx.SuccessResult("[" + result + "]")
		ctx.Log("sql_handle_query success.")
	} else {
		ctx.ErrorResult("sql_handle_query failure: " + tblinfo)
		ctx.Log("sql_handle_query [end]")
	}
}

//export sql_handle_query_one
func sql_handle_query_one() {
	LogMessage("[zitao] ========================================start")
	LogMessage("[zitao] input func: sql_handle_query_one")

	status, _ := ArgString("status")
	tblinfo, _ := ArgString("tblinfo")
	LogMessage("[zitao] change sql_handle_query_one[status]: " + status)
	LogMessage("[zitao] change sql_handle_query_one[tblinfo]: " + tblinfo)
	ctx := NewSqlSimContext()

	tblinfo_list := strings.Split(tblinfo, "##")

	var result string
	for _, tblinfo_once := range tblinfo_list {
		ec, resultCode := ctx.ExecuteQueryOne(tblinfo_once)
		if resultCode != SUCCESS {
			ctx.Log("ExecuteQueryOne error")
			ctx.ErrorResult("ExecuteQueryOne error")
			return
		}

		jsonStr := ec.ToJson()
		ctx.Log("NextRow: " + jsonStr)
		result += ","
		result += jsonStr
	}
	result = strings.TrimLeft(result, ",")
	if status == "success" {
		ctx.SuccessResult("[" + result + "]")
		ctx.Log("sql_handle_query_one success.")
	} else {
		ctx.ErrorResult("sql_handle_query_one failure: " + tblinfo)
		ctx.Log("sql_handle_query_one [end]")
	}
}

func main() {

}
