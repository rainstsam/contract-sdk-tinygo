package main

import (
	"chainmaker.org/contract-sdk-tinygo/sdk/convert"
)

// 安装合约时会执行此方法，必须
//export init_contract
func initContract() {
}

// 升级合约时会执行此方法，必须
//export upgrade
func upgrade() {
}

//export increase
func increase() {
	var heartbeatInt int32 = 0
	if heartbeatString, resultCode := GetState("Counter", "heartbeat"); resultCode != SUCCESS {
		heartbeatInt = 1
		LogMessage("call increase, put state from empty")
		PutState("Counter", "heartbeat", convert.Int32ToString(heartbeatInt))
	} else {
		heartbeatInt, _ = convert.StringToInt32(heartbeatString)
		heartbeatInt++
		LogMessage("call increase, put state from exist")
		PutState("Counter", "heartbeat", convert.Int32ToString(heartbeatInt))

	}
	SuccessResult(convert.Int32ToString(heartbeatInt))
}

//export query
func query() {
	LogMessage("call query")
	if heartbeatString, resultCode := GetState("Counter", "heartbeat"); resultCode != SUCCESS {
		ErrorResult("failed to query state")
	} else {
		LogMessage("call query, heartbeat:" + heartbeatString)
		SuccessResult(heartbeatString)
	}
}

//type person struct {
//	Name string `json:"name"`
//	Age  int64  `json:"age"`
//}
//
////export for_dag
//func for_dag() {
//	if txId, resultCode := GetTxId(); resultCode != SUCCESS {
//		ErrorResult("failed to get tx id")
//	} else {
//		for i := 0; i < 2; i++ {
//			PutState("test", txId[i:i+1], "value")
//		}
//		SuccessResult("ok")
//	}
//}
//
////export json_ex
//func json_ex() {
//	if num, resultCode := ArgString("num"); resultCode != SUCCESS {
//		ErrorResult("failed to get num")
//		return
//	} else {
//		LogMessage("num " + num.(string))
//	}
//	m := Args()
//	LogMessage("m[\"str\"] = " + m["str"].(string))
//	var sth map[string]interface{}
//	sth = m["sth"].(map[string]interface{})
//	LogMessage("m[\"sth\"][\"num1\"] = " + strconv.FormatInt(sth["num1"].(int64), 10))
//	LogMessage("m[\"sth\"][\"num2\"] = " + strconv.FormatInt(sth["num2"].(int64), 10))
//	LogMessage("m[\"sth\"][\"str1\"] = " + sth["str1"].(string))
//	LogMessage("m[\"sth\"][\"str2\"] = " + sth["str2"].(string))
//	var persons []interface{}
//	persons = sth["persons"].([]interface{})
//	var person1 = persons[0].(map[string]interface{})
//	var person2 = persons[1].(map[string]interface{})
//	var p1, p2 person
//	p1.Name = person1["name"].(string)
//	p1.Age = person1["age"].(int64)
//	p2.Name = person2["name"].(string)
//	p2.Age = person2["age"].(int64)
//	LogMessage("person1: " + p1.Name + " age " + strconv.FormatInt(p1.Age, 10))
//	LogMessage("person2: " + p2.Name + " age " + strconv.FormatInt(p2.Age, 10))
//	// _, err := strconv.ParseInt("-1", 10, 64)
//	// if err != nil {
//	// 	LogMessage(err.Error())
//	// }
//	// strconv.FormatInt(1e9, 10)
//}
//
////export add
//func add() {
//	data1 := 0
//	data2 := 0
//	if dataString, resultCode := ArgString("num1"); resultCode != SUCCESS {
//		LogMessage("failed to get num1")
//	} else {
//		data1, _ = strconv.Atoi(dataString.(string))
//		LogMessage(dataString.(string))
//	}
//	if dataString, resultCode := ArgString("num2"); resultCode != SUCCESS {
//		LogMessage("failed to get num2")
//	} else {
//		data2, _ = strconv.Atoi(dataString.(string))
//		LogMessage(dataString.(string))
//	}
//
//	SuccessResult("Result:" + strconv.Itoa(int(int64(data1)+int64(data2))))
//}
//
////export save_only_key
//func saveFromKey() {
//	txId, _ := GetTxId()
//	time, _ := ArgString("time")
//	fileHash, _ := ArgString("file_hash")
//	fileName, _ := ArgString("file_name")
//
//	PutStateFromKey(fileHash, txId+" "+time+" "+fileHash+" "+fileName)
//	SuccessResult("ok")
//}
//
////export find_by_file_hash_only_key
//func findByFileHashFromKey() {
//	fileHash, _ := ArgString("file_hash")
//	if bytes, resultCode := GetStateFromKey(fileHash); resultCode != SUCCESS {
//		ErrorResult("failed to call get_state, only 64 letters and numbers are allowed. got key:" + "fact" + ", field:" + fileHash)
//		return
//	} else {
//		LogMessage("get val:" + string(bytes))
//		SuccessResult(string(bytes))
//	}
//}
//
////export call_contract_test
//func callContractTest() {
//	LogMessage("====================call_contract_test start====================")
//	contractName, _ := ArgString("contract_name")
//	method, _ := ArgString("method")
//	fileHash, _ := ArgString("file_hash")
//	fileName, _ := ArgString("file_name")
//	param := make(map[string]string)
//	param["file_hash"] = fileHash
//	param["file_name"] = fileName
//	LogMessage("file_hash=" + fileHash + " file_name=" + fileName)
//	CallContract(contractName, method, param)
//	CallContract(contractName, "increase", param)
//	LogMessage("====================call_contract_test done====================")
//}
func main() {

}
