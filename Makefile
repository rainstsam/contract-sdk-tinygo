build:
	tinygo build -no-debug -opt=s -o main.wasm -target wasm

fact: build
	cp ./main.wasm ../chainmaker-go/test/wasm/go-fact-2.0.0.wasm

sql: build
	cp ./main.wasm ../chainmaker-go/test/wasm/go-sql-2.0.0.wasm

verify: build
	cp ./main.wasm ../chainmaker-go/test/wasm/go-func-verify-2.0.0.wasm
	cp ./main.wasm ../chainmaker-go/test/wasm/go-fact-2.0.0.wasm
	cp ./main.wasm ../chainmaker-go/test/wasm/go-counter-2.0.0.wasm

sql-perf: build
	cp ./main.wasm ../chainmaker-go/test/send_proposal_request_sql/perf/go-sql-perf-2.0.0.wasm

